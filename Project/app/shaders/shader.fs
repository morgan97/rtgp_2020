// the following shader is used for the part of the project of bloom 
// for rendering the scene with shadow we use another fr. shader = shader_shadow.fs
// it is the same but it includes also the computation of shadows 
#version 410 core 

// if not specified, the default outColor is FragColor 
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct MaterialTexture {
    vec3 specular;
    float shininess;
};

struct Light {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform MaterialTexture materialTexture;
uniform Material material;
uniform Light light;

uniform sampler2D diffuseTex;

uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform float repetition;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;


////////////////// SUBROUTINE STAFF ////////////////
// the "type" of the Subroutine
subroutine vec3 ill_model();
subroutine uniform ill_model Illumination_Model_ML;

// a subroutine for the Blinn-Phong model for multiple lights
subroutine(ill_model)
vec3 drawLight() 
{   
    // we draw the light completly white 
    // other object in the scene need to be influenced by the effects of the light 
    return lightColor;
}


subroutine(ill_model)
vec3 drawNoTex()
{
    // it uses only the phong and not the blinn phong 
    // ambient
    vec3 ambient = material.ambient * light.ambient;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse  = light.diffuse * (diff * material.diffuse);

    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular); 


    vec3 result = ambient + diffuse + specular;
    return result;
}


subroutine(ill_model)
vec3 Blinn_Phong()
{
    // create tex 
    vec2 repeated_Tex = mod(TexCoords*repetition, 1.0);
    vec4 surfaceColor = texture(diffuseTex, repeated_Tex);

    // ambient
    vec3 ambient = light.ambient * surfaceColor.xyz;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse =  light.diffuse * diff * surfaceColor.xyz;

    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(norm, halfwayDir), 0.0), materialTexture.shininess);
    vec3 specular = light.specular * (spec * materialTexture.specular); 


    vec3 result = ambient + diffuse + specular;

    return result;
}


void main (void){
    // colorFrag = vec4(colorIn, 1.0);
    vec3 result = Illumination_Model_ML(); 

    // with the dot product we transform the values in a grey scale 
    float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));
    // then we check if the light emitted is major than 1.0
    if(brightness > 1.0)
        BrightColor = vec4(result, 1.0);    // we return the real color of the fragment   
    else
        BrightColor = vec4(0.0, 0.0, 0.0, 1.0); // else we return black 
    FragColor = vec4(result, 1.0);
}