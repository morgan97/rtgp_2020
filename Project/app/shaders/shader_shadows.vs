#version 410 core 

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 lightSpaceMatrix;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;
out vec4 FragPosLightSpace;

void main (){
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position,1.0f);
    // Normal = normalize(normalMatrix * aNormal);
    Normal = mat3(transpose(inverse(modelMatrix))) * aNormal;
    FragPos = vec3(modelMatrix * vec4(position, 1.0));
    TexCoords = aTexCoords;
    // --------------------------------------
    // we transform the world space vertex positions to light space for use them in the fragment shader 
    FragPosLightSpace = lightSpaceMatrix * vec4(FragPos, 1.0);
}