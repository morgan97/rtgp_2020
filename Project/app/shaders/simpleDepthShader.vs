#version 410 core 
layout (location = 0) in vec3 aPos;

uniform mat4 lightSpaceMatrix;
uniform mat4 modelMatrix;

void main ()
{
    // we render all the world from the point of view of the light.
    // we multiply objects just for lightSpaceMatrix that contains all the info 
    // for projection and view from the light point of view 
    gl_Position = lightSpaceMatrix * modelMatrix * vec4(aPos, 1.0);
}