#version 410 core 

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 Normal;
out vec3 FragPos;
out vec2 TexCoords;

void main (){
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position,1.0f);
    // Normal = normalize(normalMatrix * aNormal);

    // ------------------------------------------------------------
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    // ------------------------------------------------------------

    /*
        we need to transform the Normal from local coordinates to 
        world coordinates but this is more complicated than with fragPos.
        1. normal vectors are just direction vectors and do not represent a specific position
        2. normal vectors do not have a homogeneous coordinate 
        So we cannot apply translation or scaling because the normals would not be more 
        perpendicular to the surface

        To fix it we need to use a different model matrix specifically tailored for normal vectors 
        The matrix takes the name of NORMAL MATRIX and uses few linear algebraic operations.
    */
    
    Normal = mat3(transpose(inverse(modelMatrix))) * aNormal;

    /* 
        Demonstration

        Like we said, if we operate a scaling, normals are not preserved because, 
        differently from the tangent, normals change.
        So, our goal is to find a 3x3 matrix that if multiplied by n return the correct normal.
        How do we do that ? 

        N.T = 0 because tangent and normal are perpendicular for definition 
        so at the end, after the transformation we need to have that 
        N'.T' = 0 
        
        let's consider a matrix G that multiplied for the normal return the correct value for the new normals. 
        (GN).(MT) = 0 because T is modified by M, the model matrix, but at the end the dot product is always zero 

        for linear algebra we know that the dot product can be transformed into a product of vectors so ...
        (GN).(MT) = (GN)transpose * (MT)
        but for linear algebra yet we know that the transpose of a multiplication in the multiplication of the transpose ->
        (GN)transpose * (MT) = (N)transpose (G)transpose (MT)

        (G)transpose M is the identity matrix =====> G = ((M)inverse)transpose
    */ 

    // ------------------------------------------------------------
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    // ------------------------------------------------------------
    FragPos = vec3(modelMatrix * vec4(position, 1.0));
    TexCoords = aTexCoords;
}