#version 410 core 

layout (location = 0) out vec4 FragColor;


struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};

struct MaterialTexture {
    vec3 specular;
    float shininess;
};

struct Light {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform MaterialTexture materialTexture;
uniform Material material;
uniform Light light;

uniform sampler2D diffuseTex;
uniform sampler2D shadowMap;

uniform vec3 lightColor;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform float repetition;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;
in vec4 FragPosLightSpace;


////////////////// SUBROUTINE STAFF ////////////////
// the "type" of the Subroutine
subroutine vec3 ill_model();
subroutine uniform ill_model Illumination_Model_ML;

// a subroutine for the Blinn-Phong model for multiple lights
subroutine(ill_model)
vec3 drawLight() 
{   
    // we draw the light completly white 
    // other object in the scene need to be influenced by the effects of the light 
    return lightColor;
}


subroutine(ill_model)
vec3 drawNoTex()
{
    // it uses only the phong and not the blinn phong 
    vec3 ambient = material.ambient * light.ambient;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse  = light.diffuse * (diff * material.diffuse);

    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * (spec * material.specular); 


    vec3 result = ambient + diffuse + specular;
    return result;
}

float ShadowCalculation (vec4 fragPosLightSpace)
{
    // perform perspective divide
    // Open GL performs automatically perspective divide
    // As fragPosLightSpace is not passed to the fragment shader through gl_Position
    // we have to do this perspective divide ourselves 
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;  // this returns the fragment's light-space position in the range [-1,1]
    // NDC coordinates to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r; // we access the first value for the color {r, g, b, a} after texture application (I suppose)
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;  
    // check whether current frag pos is in shadow
    vec3 normal = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    // removing shadow acne 
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);  
    // over sampling and PCF
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            // if 0.0 fragment is not in shadow else if 1.0 fragment is in shadow 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    
    // keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoords.z > 1.0)
        shadow = 0.0;
        
    return shadow;
}


subroutine(ill_model)
vec3 Blinn_Phong()
{
    // textures based on the number of the repetitions 
    vec2 repeated_Tex = mod(TexCoords*repetition, 1.0);
    // creation of the tex
    vec4 surfaceColor = texture(diffuseTex, repeated_Tex);

    // --------------------------------------------------------
    // --------------------------------------------------------
    // --------------------------------------------------------


    // light can scatter and bounce in many directions 
    // reaching spots not visible
    // ambient
    vec3 ambient = light.ambient * surfaceColor.xyz;

    // diffuse -> lambertian 
    // normalize = to get unit vector 
    vec3 norm = normalize(Normal);
    // ray of the light 
    vec3 lightDir = normalize(lightPos - FragPos);
    // we take the dot product, max is used because if the angle 
    // is major than 90° we obtain a negative value
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse =  light.diffuse * diff;

    // specular -> Blinn phong model 
    float specularStrength = 0.5;
    // get the view direction from where we are 
    vec3 viewDir = normalize(viewPos - FragPos);
    // in the middle of the viewer and the light position 
    vec3 halfwayDir = normalize(lightDir + viewDir);
    // get the reflected ray from where we are 
    vec3 reflectDir = reflect(-lightDir, norm);
    // get specular component = dot product bwtween normal and halfway ray
    float spec = pow(max(dot(norm, halfwayDir), 0.0), materialTexture.shininess);
    vec3 specular = spec * materialTexture.specular; 

    // shadow computation 
    float shadow = ShadowCalculation(FragPosLightSpace);
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * surfaceColor.xyz; 

    return lighting;
}


void main (void){
    // colorFrag = vec4(colorIn, 1.0);
    vec3 result = Illumination_Model_ML(); 
    FragColor = vec4(result, 1.0);
}