// the shader follows the tutorial https://greentec.github.io/shadertoy-fire-shader-en/

#version 410 core

uniform vec2 resolution;
uniform float time;
uniform bool choose_color_fire;
in vec3 FragPos;
out vec4 FragColor;

#define timeScale 			time * 1.0
#define fireMovement 		vec2(-0.01, -0.5)
#define distortionMovement	vec2(-0.01, -0.3)
#define normalStrength		40.0
#define distortionStrength	0.1

// the output is a 2Dvector with values between -1 and 1 
vec2 hash( vec2 p ) {
	p = vec2( dot(p,vec2(127.1,311.7)), dot(p,vec2(269.5,183.3)) );
    // fract stands for fraction, it returns only the fractional part
	return -1.0 + 2.0*fract(sin(p) * 43758.5453123);
}
// we create a randomly noise with an unpredictable hash
// simplex noise 
// it interpolates values between three verteces of a triangle  
float noise( in vec2 p ) {
    const float K1 = 0.366025404; // (sqrt(3)-1)/2;
    const float K2 = 0.211324865; // (3-sqrt(3))/6;

	vec2 i = floor( p + (p.x+p.y) * K1 );

    vec2 a = p - i + (i.x+i.y) * K2;
    vec2 o = step(a.yx,a.xy);
    vec2 b = a - o + K2;
	vec2 c = a - 1.0 + 2.0*K2;

    vec3 h = max( 0.5-vec3(dot(a,a), dot(b,b), dot(c,c) ), 0.0 );

	vec3 n = h*h*h*h*vec3( dot(a,hash(i+0.0)), dot(b,hash(i+o)), dot(c,hash(i+1.0)));

    return dot( n, vec3(70.0) );
}

// it is not practical to use just noise because it is blurred 
// fbm reduce this effect and give us a detailed image
// fractal Brownian Motion = noise(x) + 1/2 noise(2x) + 1/4 noise(4x) + ... + 1/n noise(nx); 
float fbm ( in vec2 p ) {
    float f = 0.0;
    mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
    f  = 0.5000*noise(p); p = m*p;
    f += 0.2500*noise(p); p = m*p;
    f += 0.1250*noise(p); p = m*p;
    f += 0.0625*noise(p); p = m*p;
    f = 0.5 + 0.5 * f;
    return f;
}

// we use it to emulate the presence of a roughness surface 
// we store the x y z value of the normals in the RGB channels 
vec3 bumpMap(vec2 uv) {
    vec2 s = 1. / resolution.xy;
    float p =  fbm(uv);
    float h1 = fbm(uv + s * vec2(1., 0));
    float v1 = fbm(uv + s * vec2(0, 1.));

   	vec2 xy = (p - vec2(h1, v1)) * normalStrength;
    return vec3(xy + .5, 1.);
}
void main() {
    vec2 uv = FragPos.xy/resolution.xy;
    // we modify the normal using the bump map and it is affected by time 
    vec3 normal = bumpMap(uv * vec2(1.0, 0.3) + distortionMovement * timeScale);
    // if displacement is used in 2d it creates the effect of a wave 
    // clamp limits the output between the minumum and the maximum values after multiplying by distortionstrength 
    vec2 displacement = clamp((normal.xy - .5) * distortionStrength, -1., 1.);
    uv += displacement; 

    vec2 uvT = (uv * vec2(1.0, 0.5)) + fireMovement * timeScale;
    // it is elevated by 1. so it doesn't effect, but if we modify we can obtain a darker image 
    float n = pow(fbm(8.0 * uvT), 1.0);

    float gradient = pow(1.0 - uv.y, 2.0) * 5.;
    float finalNoise = n * gradient;


    // we get the colors multipling the noise per n times the color we want 
    vec3 color_red = finalNoise * vec3(2.*n, 2.*n*n*n, n*n*n*n);
    vec3 color_blue = finalNoise * vec3(n*n*n*n, 2.*n*n*n, 2.*n);
    if (choose_color_fire)
        FragColor = vec4(color_red, 1.0);
    else 
        FragColor = vec4(color_blue, 1.0);
}