/*
    PROJECT -> REAL TIME GRAPHICS PROGRAMMING 
    AUTHOR -> MORGAN MALAVASI 
    MAT. 960552

    The class represents a third person camera system 
*/

#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <iostream>
#include <cmath>

enum Camera_Movement
{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 13.5f;
const float SPEED_ROTATION = 6.5f;

const float SENSITIVITY = 0.1f;

// variable for the camera distance from actor
// ------------------------------------------
const float mHorzDist = 22.0f;
const float mVertDist = 7.0f;
const float mTargetDist = 5.0f;
// ------------------------------------------

// constraint to not exit from the battlefield
const float block = 100.0f;

class CameraThird
{
public:
    // camera info
    glm::vec3 Position;
    glm::vec3 IdealPosition;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 RightLeft;
    glm::vec3 WorldUp;

    // player info
    glm::vec3 ownerPos;
    glm::vec3 ownerForward;
    glm::vec3 ownerUp;
    glm::vec3 targetPosition;
    float rotateActor = 0.0f;

    // rotation and movement info
    float Yaw;
    float Pitch;
    // camera options
    float MovementSpeed;
    float MovementRotation;
    float MouseSensitivity;

    // camera constructor 
    CameraThird(glm::vec3 actorPos)
    {
        ownerPos = actorPos;
        ownerForward = glm::vec3(0.0f, 0.0f, -1.0f);
        ownerUp = glm::vec3(0.0f, 1.0f, 0.0f);

        WorldUp = glm::vec3(0.0f, 1.0f, 0.0f);

        Yaw = YAW;
        MouseSensitivity = SENSITIVITY;
        MovementSpeed = SPEED;
        MovementRotation = SPEED_ROTATION;
        updateCameraVectors();
    }

    void ProcessKeyboard(Camera_Movement direction, float deltaTime)
    {   
        // movement of the camera is based on deltaTime
        float velocity = MovementSpeed * deltaTime;
        float mRotation = MovementRotation * deltaTime * 12;
        
        if (direction == FORWARD)
        {   
            // save current position
            float last_x = ownerPos.x;
            float last_z = ownerPos.z;

            // modify position 
            ownerPos.x += ownerForward.x * velocity;
            ownerPos.z += ownerForward.z * velocity;

            // constraint
            // check if the actor is out of the battlefield 
            if (ownerPos.x >= block-3.0f || ownerPos.x <= -(block)+3.0f)
                ownerPos.x = last_x;

            if (ownerPos.z <= -(block)+3.0f || ownerPos.z >= block-3.0f)
                ownerPos.z = last_z;
            
            // std::cout << "posizione dell'attore -> " << ownerPos.z << " BLOCK = " << block << std::endl;
        }
        if (direction == BACKWARD)
        {
            float last_x = ownerPos.x;
            float last_z = ownerPos.z;

            ownerPos.x -= ownerForward.x * velocity;
            ownerPos.z -= ownerForward.z * velocity;

            if (ownerPos.x >= block-3.0f || ownerPos.x <= -(block)+3.0f)
                ownerPos.x = last_x;

            if (ownerPos.z <= -(block)+3.0f || ownerPos.z >= block-3.0f)
                ownerPos.z = last_z;

        }
        if (direction == LEFT)
        {   
            // if we want to go left we need to update the Yaw 
            Yaw -= mRotation;
            // when rotating the camera, also the actor updates his rotation 
            rotateActor += mRotation;
            // direction is given using the cosine and the sin of the Yaw angle 
            ownerForward.x = cos(glm::radians(Yaw));
            ownerForward.z = sin(glm::radians(Yaw));
        }
        if (direction == RIGHT)
        {
            Yaw += mRotation;
            rotateActor -= mRotation;
            ownerForward.x = cos(glm::radians(Yaw));
            ownerForward.z = sin(glm::radians(Yaw));
        }

        // set up the position of the camera to a constant value 
        // given by mVertDist that is the distance from the ground
        Position.y = ownerUp.y * mVertDist;

        updateCameraVectors();
    }

    // mouse events are not used in the project 
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constraintPitch = true)
    {

        updateCameraVectors();
    }

    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(Position, targetPosition, WorldUp);
    }

private:
    // glm::vec3 rotDirection;
    void updateCameraVectors()
    {
        // for a third person camera we need to determine the position of the camera respect an object
        // and also a target position

        // CAMERA POSITION = OWNER POSITION - OWNER FORWARD * HDist + OWNER UP * VDist
        // where HDist is the horizontal distance between the actor and the camera position
        // while VDist is the vertical distance
        Position = ownerPos;
        Position -= ownerForward * mHorzDist;
        Position += ownerUp * mVertDist;

        // TARGET POS = OWNER POSITION + OWNER FORWARD * TargetDist
        // Where target dist is the difference between the camera pos and the target pos
        targetPosition = ownerPos + ownerForward * mTargetDist;
    }
};