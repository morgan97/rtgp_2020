/*
    PROJECT -> REAL TIME GRAPHICS PROGRAMMING 
    AUTHOR -> MORGAN MALAVASI 
    MAT. 960552

    The file is composed by four classes:
    - BoneTransform 
    - Skeleton 
    - Animation 
    - MatrixPal
*/

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include "rapidjson/document.h"
#include "mesh_v1.h"
#include "../include/glm/glm.hpp"
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

const size_t MAX_SKELETON_BONES = 96;

/*
    Bonetransform is a class for representing the rotation and the translation of bones
*/
class BoneTransform
{
public:
    // we use quaternion because quaternions are usefull for the interpolation.
    glm::quat mRotation;
    glm::vec3 mTranslation;

    // the function returns a matrix for mRotation and mTranslation given 
    glm::mat4 toMatrix()
    {   
        // toMat4 Convert quaternions in a mat4 
        glm::mat4 rot = glm::toMat4(mRotation);

        glm::mat4 trans = glm::mat4(1.0f);
        trans[3][0] = mTranslation.x;
        trans[3][1] = mTranslation.y;
        trans[3][2] = mTranslation.z;

        // we apply the rotation and the transformation 
        glm::mat4 oper = trans * rot;

        return oper;
    }

    // it interpolates two BoneTranform to obtain another 
    static BoneTransform interpolate(const BoneTransform &a, const BoneTransform &b, float f)
    {
        BoneTransform retVal;
        // it interpolates two quaternion values 
        retVal.mRotation = glm::slerp(a.mRotation, b.mRotation, f);
        // it interpolates two translations 
        retVal.mTranslation = lerp(a.mTranslation, b.mTranslation, f);
        return retVal;
    }

private:
    static glm::vec3 lerp(const glm::vec3 &a, const glm::vec3 &b, float f)
    {
        return glm::vec3(a + f * (b - a));
    }
};


/*  
    Skeleton loads the skeleton from the CatWarrior.gpskel file
*/
class Skeleton
{
public:
    // every pose is given by the BoneTransform matrix, the name and the parent
    struct Bone
    {
        BoneTransform mLocalBindPose;
        std::string mName;
        int mParent;
    };
    // vector for all the bones
    std::vector<Bone> mBones;

    // vector for all the inverse matrix
    std::vector<glm::mat4> mGlobalInvBindPoses;

    Skeleton(const std::string &filename)
    {
        // check if the file exists, if not found return error and display error
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cout << "File not found: Skeleton " + filename << std::endl;
            exit(-1);
        }

        // read buffer stream
        std::stringstream fileStream;
        fileStream << file.rdbuf();
        std::string contents = fileStream.str();
        rapidjson::StringStream jsonStr(contents.c_str());
        rapidjson::Document doc;
        doc.ParseStream(jsonStr);

        // check if the doc is an object
        if (!doc.IsObject())
        {
            std::cout << "Skeleton " + filename + " not valid json " << std::endl;
            exit(-1);
        }

        int boneCount = doc["bonecount"].GetInt();
        mBones.reserve(boneCount);

        const rapidjson::Value &bones = doc["bones"];
        
        // termporal storage
        Bone temp;  

        for (rapidjson::SizeType i = 0; i < boneCount; i++)
        {
            // save name
            const rapidjson::Value &name = bones[i]["name"];
            temp.mName = name.GetString();

            // save parent
            const rapidjson::Value &parent = bones[i]["parent"];
            temp.mParent = parent.GetInt();

            // save bindPose
            const rapidjson::Value &bindPose = bones[i]["bindpose"];
            if (!bindPose.IsObject())
            {
                std::cout << "Skeleton " + filename + " is invalid " << std::endl;
                exit(-1);
            }

            const rapidjson::Value &rot = bindPose["rot"];
            const rapidjson::Value &trans = bindPose["trans"];

            // get the values for the rotation
            temp.mLocalBindPose.mRotation.x = static_cast<float>(rot[0].GetDouble());
            temp.mLocalBindPose.mRotation.y = static_cast<float>(rot[1].GetDouble());
            temp.mLocalBindPose.mRotation.z = static_cast<float>(rot[2].GetDouble());
            temp.mLocalBindPose.mRotation.w = static_cast<float>(rot[3].GetDouble());
            // get the values for the translation
            temp.mLocalBindPose.mTranslation.x = static_cast<float>(trans[0].GetDouble());
            temp.mLocalBindPose.mTranslation.y = static_cast<float>(trans[1].GetDouble());
            temp.mLocalBindPose.mTranslation.z = static_cast<float>(trans[2].GetDouble());

            mBones.emplace_back(temp);
        }

        // once loaded the skeleton model we need to compute the inverse matrices
        computeGlobalInverseBindPose();
    }

    // return the vector of bones 
    const std::vector<Bone> &GetBones() const
    {
        return mBones;
    }

    // get the vector of inverted matrices 
    const std::vector<glm::mat4> &GetGlobalInvBindPoses() const
    {
        return mGlobalInvBindPoses;
    }

    // get num of bones
    size_t GetNumBones() const
    {
        return mBones.size();
    }

private:
    // it computes all the inverse bind poses 
    void computeGlobalInverseBindPose()
    {
        mGlobalInvBindPoses.resize(mBones.size());

        // compute global bind pose for each bone
        // the global bind pose for the root is also the local bond pose
        mGlobalInvBindPoses[0] = mBones[0].mLocalBindPose.toMatrix();

        // for each remaining bone we need to compute its global bind pose
        // that is given by the parent's global pose multiplied for its local bind pose
        for (size_t i = 1; i < mGlobalInvBindPoses.size(); i++)
        {
            glm::mat4 localMat = mBones[i].mLocalBindPose.toMatrix();
            mGlobalInvBindPoses[i] = mGlobalInvBindPoses[mBones[i].mParent] * localMat;
        }
        // at this stage mGlobalInvBindPoses contains all the global bind poses.
        // if we want the obatain then we invert all the matrices
        for (int i = 0; i < mGlobalInvBindPoses.size(); i++)
        {
            mGlobalInvBindPoses[i] = glm::inverse(mGlobalInvBindPoses[i]);
        }
    }
};

/*
    Load info about the Animations 
    Each animation is composed by tracks in which every bone has a lot of transformation for each frame 
*/
class Animation
{
public:
    unsigned int mNumBones;
    unsigned int mNumFrames;
    // matrix where bones are the rows and trasformations are the columns 
    std::vector<std::vector<BoneTransform>> mTracks;

    Animation(const std::string &filename)
    {
        // check if the file exists, if not found return error and display error
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cout << "File not found: Animation " + filename << std::endl;
            exit(-1);
        }

        // read buffer stream
        std::stringstream fileStream;
        fileStream << file.rdbuf();
        std::string contents = fileStream.str();
        rapidjson::StringStream jsonStr(contents.c_str());
        rapidjson::Document doc;
        doc.ParseStream(jsonStr);

        // check if the doc is an object
        if (!doc.IsObject())
        {
            std::cout << "Animation file " + filename + " not valid json " << std::endl;
            exit(-1);
        }

        // read sequence
        const rapidjson::Value &sequence = doc["sequence"];

        const rapidjson::Value &frames = sequence["frames"];
        const rapidjson::Value &length = sequence["length"];
        const rapidjson::Value &boneCount = sequence["bonecount"];

        if (!frames.IsUint() || !length.IsDouble() || !boneCount.IsUint())
        {
            std::cout << "Animation Data " + filename + " not valid " << std::endl;
            exit(-1);
        }
        // info about the animation
        mNumFrames = frames.GetUint();
        mDuration = length.GetDouble();
        mNumBones = boneCount.GetUint();
        mFrameDuration = mDuration / (mNumFrames - 1);

        mTracks.resize(mNumBones);

        const rapidjson::Value &tracks = sequence["tracks"];

        // save tracks info
        for (rapidjson::SizeType i = 0; i < tracks.Size(); i++)
        {
            // retrieve bone number
            size_t boneIndex = tracks[i]["bone"].GetUint();
            // transforms array values
            const rapidjson::Value &transforms = tracks[i]["transforms"];

            BoneTransform temp;

            for (rapidjson::SizeType j = 0; j < transforms.Size(); j++)
            {   
                // get rotation for the bone of indez boneIndex
                const rapidjson::Value &rot = transforms[j]["rot"];
                // get translation for the bone of indez boneIndex
                const rapidjson::Value &trans = transforms[j]["trans"];

                // check if they are arrays 
                if (!rot.IsArray() || !trans.IsArray())
                {
                    std::cout << "Animation Data / ROT / TRANS " + filename + " not valid " << std::endl;
                    exit(-1);
                }

                // get the values for the rotation
                temp.mRotation.x = static_cast<float>(rot[0].GetDouble());
                temp.mRotation.y = static_cast<float>(rot[1].GetDouble());
                temp.mRotation.z = static_cast<float>(rot[2].GetDouble());
                temp.mRotation.w = static_cast<float>(rot[3].GetDouble());

                // get the values for the translation 
                temp.mTranslation.x = static_cast<float>(trans[0].GetDouble());
                temp.mTranslation.y = static_cast<float>(trans[1].GetDouble());
                temp.mTranslation.z = static_cast<float>(trans[2].GetDouble());

                mTracks[boneIndex].emplace_back(temp);
            }
        }
    }

    // the function takes in an empty 4x4 matrices vector, a skeleton and a time. 
    // the role is to fill the outPoses vector with global matrices for a specific time.
    void getGlobalPoseAtTime(std::vector<glm::mat4> &outPoses, const Skeleton inSkeleton, float inTime)
    {
        // Resize the outPoses vector if needed
        if (outPoses.size() != mNumBones)
        {
            outPoses.resize(mNumBones);
        }

        // given the time "inTime" we are in, we take the previous and next frame. 
        size_t frame = static_cast<size_t>(inTime / mFrameDuration);
        size_t nextFrame = frame + 1;
        // pct is between 0 and 1 and represents the exact state bwtwween two frames
        float pct = inTime / mFrameDuration - frame;
        
        // not all the tracks need to have elements inside 
        // so we must check the size 
        if (mTracks[0].size() > 0)
        {
            // because we want the exact state between two frame we interpolate between them 
            BoneTransform interp = BoneTransform::interpolate(mTracks[0][frame], mTracks[0][nextFrame], pct);
            outPoses[0] = interp.toMatrix();
        }
        else
        {
            outPoses[0] = glm::mat4(1.0f);
        }

        // get vector of bones
        const std::vector<Skeleton::Bone> &bones = inSkeleton.GetBones();

        for (size_t bone = 1; bone < mNumBones; bone++)
        {
            glm::mat4 localMat = glm::mat4(1.0f);
            if (mTracks[bone].size() > 0)
            {
                BoneTransform interp = BoneTransform::interpolate(mTracks[bone][frame],
                                                                  mTracks[bone][nextFrame], pct);
                localMat = interp.toMatrix();
            }
            outPoses[bone] = outPoses[bones[bone].mParent] * localMat;
        }
    }

    // return FrameDuration
    float getFrameDuration() { return mFrameDuration; }
    // return Duration
    float getDuration() { return mDuration; }

private:
    float mFrameDuration;
    float mDuration;
};


/*
    It creates the matrices to load in the shader to modify the catWarrior model
*/
class MatrixPal
{
public:
    glm::mat4 MatrixPalette[MAX_SKELETON_BONES];

    void computeMatrixPalette(const Skeleton inSkeleton, Animation anim)
    {
        // get the inverse binding poses
        const std::vector<glm::mat4> &globalInvBindPoses = inSkeleton.GetGlobalInvBindPoses();

        std::vector<glm::mat4> currentPoses;

        // compute the poses for a given time in the animation 
        anim.getGlobalPoseAtTime(currentPoses, inSkeleton, mAnimTime);

        // setup palette for each bone
        for (size_t i = 0; i < inSkeleton.GetNumBones(); i++)
        {
            // get back to local space cootdinates 
            MatrixPalette[i] = currentPoses[i] * globalInvBindPoses[i];
        }
    }

    // if time of the animation exceed return back 
    void updateTime(float deltaTime, Animation anim)
    {
        mAnimTime += deltaTime * mAnimPlayRate;
        while (mAnimTime > anim.getDuration())
        {
            mAnimTime -= anim.getDuration();
        }
    }

private:
    float mAnimTime = 0;
    float mAnimPlayRate = 1.0f;
};