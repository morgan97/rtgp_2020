/*
    PROJECT -> REAL TIME GRAPHICS PROGRAMMING 
    AUTHOR -> MORGAN MALAVASI 
    MAT. 960552
*/

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <glad/glad.h>
#include <glfw/glfw3.h>
#include "rapidjson/document.h"
#include "mesh_v1.h"
#include "../include/glm/glm.hpp"

// data structure for vertices
struct VertexRapidJson
{
    // vertex coordinates
    glm::vec3 Position;
    // Normal
    glm::vec3 Normal;
    // Texture coordinates
    glm::vec2 TexCoords;
    // Tangent
    glm::vec3 Tangent;
    // Bitangent
    glm::vec3 Bitangent;
};

/////////////////// MESH class ///////////////////////
class MeshRapidJson
{
public:
    // data structures for vertices, and indices of vertices (for faces)
    vector<VertexRapidJson> vertices;
    vector<GLuint> indices;
    // VAO
    GLuint VAO;

    //////////////////////////////////////////
    // Constructor
    MeshRapidJson(const std::string &filename)
    {

        // check if the file exists, if not found return error and display error
        std::ifstream file(filename);
        if (!file.is_open())
        {
            std::cout << "FIle not found: Mesh %s " + filename << std::endl;
            exit(-1);
        }

        // read buffer stream
        std::stringstream fileStream;
        fileStream << file.rdbuf();
        std::string contents = fileStream.str();
        rapidjson::StringStream jsonStr(contents.c_str());
        rapidjson::Document doc;
        doc.ParseStream(jsonStr);

        // check if the doc is an object
        if (!doc.IsObject())
        {
            std::cout << "Mesh " + filename + " not valid json " << std::endl;
            exit(-1);
        }

        size_t vertSize = 8;
        vector <VertexRapidJson> vertSave;

        // Load vertices
        const rapidjson::Value &vertsJson = doc["vertices"];
        if (!vertsJson.IsArray() || vertsJson.Size() < 1)
        {
            std::cout << "Mesh has no vertices " << std::endl;
            exit(-1);
        }

        std::vector<float> mVertices;
        mVertices.reserve(vertsJson.Size() * vertSize);

        for (rapidjson::SizeType i = 0; i < vertsJson.Size(); i++)
        {
            // just assume we have 8 elements
            const rapidjson::Value &vert = vertsJson[i];
            // check every line of the mesh
            if (!vert.IsArray() || vert.Size() != 8)
            {
                std::cout << "Unexpected vertex format for " + filename << std::endl;
                exit(-1);
            }

            // Add the floats
            for (rapidjson::SizeType i = 0; i < vert.Size(); i++)
            {
                // TODO -> fill the vertices struct
                mVertices.emplace_back(static_cast<float>(vert[i].GetDouble()));
                
            }

            // internal struct to load the mesh 

            VertexRapidJson v;

            // positions 
            glm::vec3 mPosition;
            mPosition.x = static_cast<float>(vert[0].GetDouble());
            mPosition.y = static_cast<float>(vert[1].GetDouble());
            mPosition.z = static_cast<float>(vert[2].GetDouble());
            v.Position = mPosition;

            // Normals 
            glm::vec3 mNormals;
            mNormals.x = static_cast<float>(vert[3].GetDouble());
            mNormals.y = static_cast<float>(vert[4].GetDouble());
            mNormals.z = static_cast<float>(vert[5].GetDouble());
            v.Normal = mNormals;

            // TexCoords
            glm::vec2 mTexCoords;
            mTexCoords.x = static_cast<float>(vert[6].GetDouble());
            mTexCoords.y = static_cast<float>(vert[7].GetDouble());
            v.TexCoords = mTexCoords;

            vertSave.push_back(v);
            
        }

        // Load the indices
        const rapidjson::Value &indJson = doc["indices"];
        if (!indJson.IsArray() || indJson.Size() < 1)
        {
            std::cout << "Mesh has no indices " << std::endl;
            exit(-1);
        }

        vector<GLuint> mIndices;
        mIndices.reserve(indJson.Size() * 3);
        for (rapidjson::SizeType i = 0; i < indJson.Size(); i++)
        {
            const rapidjson::Value &ind = indJson[i];
            if (!ind.IsArray() || ind.Size() != 3)
            {
                std::cout << "Indices format incorrect " << std::endl;
                exit(-1);
            }

            // save the three values for the indices
            mIndices.emplace_back(static_cast<GLuint>(ind[0].GetUint()));
            mIndices.emplace_back(static_cast<GLuint>(ind[1].GetUint()));
            mIndices.emplace_back(static_cast<GLuint>(ind[2].GetUint()));
        }

        // once that we load all the vertex and the index 
        // we are ready to load the mesh in memory 

        LoadMesh(vertSave, mIndices);
    }

    void LoadMesh(vector<VertexRapidJson> vertices, vector<GLuint> indices)
    {
        this->vertices = vertices;
        this->indices = indices;

        // initialization of OpenGL buffers
        this->setupMesh();
    }

    //////////////////////////////////////////

    // rendering of mesh
    void Draw()
    {
        // VAO is made "active"
        glBindVertexArray(this->VAO);
        // rendering of data in the VAO
        glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
        // VAO is "detached"
        glBindVertexArray(0);
    }

    //////////////////////////////////////////
    // buffers are deallocated when application ends
    // we do not use destructor method here:
    // destructors are called at the end of the application, or when an object has reached the end of its lifetime
    // (e.g., it goes out of scope). In this case, VAO, VBO, EBO are buffers allocated on the GPU, with copies of the data of the Mesh class instances.
    // They should stay on the GPU until no more needed (in our case, the end of the application). If we put these calls in a destructor, the destructor
    // will be called when we go out of the scope of the Mesh class, so when we go back to the scope of the Model class.
    // As a consequence, we will render a black screen, because the data on the GPU will be cancelled just after their creation.
    //  Thus, we link the deallocation to the destructor of the Model class: the Model instances are destroyed only when application ends.
    // see: https://en.cppreference.com/w/cpp/language/destructor for details
    void Delete()
    {
        glDeleteVertexArrays(1, &this->VAO);
        glDeleteBuffers(1, &this->VBO);
        glDeleteBuffers(1, &this->EBO);
    }

private:
    // VBO and EBO
    GLuint VBO, EBO;

    //////////////////////////////////////////
    // buffer objects\arrays are initialized
    // a brief description of their role and how they are binded can be found at:
    // https://learnopengl.com/#!Getting-started/Hello-Triangle
    // (in different parts of the page), or here:
    // http://www.informit.com/articles/article.aspx?p=1377833&seqNum=8
    void setupMesh()
    {
        // we create the buffers
        glGenVertexArrays(1, &this->VAO);
        glGenBuffers(1, &this->VBO);
        glGenBuffers(1, &this->EBO);

        // VAO is made "active"
        glBindVertexArray(this->VAO);
        // we copy data in the VBO - we must set the data dimension, and the pointer to the structure cointaining the data
        glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
        glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);
        // we copy data in the EBO - we must set the data dimension, and the pointer to the structure cointaining the data
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

        // we set in the VAO the pointers to the different vertex attributes (with the relative offsets inside the data structure)
        // vertex positions
        // these will be the positions to use in the layout qualifiers in the shaders ("layout (location = ...)"")
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexRapidJson), (GLvoid *)0);
        // Normals
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexRapidJson), (GLvoid *)offsetof(VertexRapidJson, Normal));
        // Texture Coordinates
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexRapidJson), (GLvoid *)offsetof(VertexRapidJson, TexCoords));
        // Tangent
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexRapidJson), (GLvoid *)offsetof(VertexRapidJson, Tangent));
        // Bitangent
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(VertexRapidJson), (GLvoid *)offsetof(VertexRapidJson, Bitangent));

        glBindVertexArray(0);
    }
};