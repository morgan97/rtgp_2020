/*
    PROJECT -> REAL TIME GRAPHICS PROGRAMMING 
    AUTHOR -> MORGAN MALAVASI 
    MAT. 960552
    
    ------------------------------------------------------------
    BUG 1 -> the app shows a little bug 
    running sometimes happens to touch some balls that are not visible
    maybe the error is in the hit value that is true or the ball is not visible 
    I'm trying yet to understand why 
    ------------------------------------------------------------
    BUG 2 -> error with viewPort, sometimes the rendering is displayed in just 1/4  
    portion of the window. Also resizing it doesn't work
    ------------------------------------------------------------
*/

// glad and glfw libraries 
#include <glad/glad.h>
#include <glfw/glfw3.h>
// stb_image library
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"
// freetype library 
#include "ft2build.h"
#include FT_FREETYPE_H
// OpenGL Math -> glm library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
// system libraries 
#include <iostream>
#include <cmath>
#include <cstdlib>
// my Class
#include "utils/shader.h"
#include "utils/model_v1.h"
#include "utils/mesh_v2.h"
#include "utils/mesh_v2_skin_loader.h"
#include "utils/mesh_v2_animation_character_loader.h"
#include "utils/camera_third_person.h"

// const distance for the objects 
#define MIN_DISTANCE 2
// size of the battlefield
// not 200 to avoid balls exit 
#define PLANE_TABLE 190.0f

// ___________ screen size of app ___________
const GLuint WIDTH = 1024, HEIGHT = 768;

// time of the engine
float deltaTime = 0.0f;
float lastFrame = 0.0f;

// mouse variable
bool polygonalMode = false;
bool firstMouse = true;
float yaw = -90.0f; // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch = 0.0f;
float lastX = WIDTH / 2.0;
float lastY = HEIGHT / 2.0;

// ___________camera position __________________
// position of the actor at the beginning of the game 
glm::vec3 catWarriorStartPos = glm::vec3(0.0f, -1.1f, -1.0f);
// start camera system
CameraThird camera(catWarriorStartPos);

// ___________ object variables ___________
// managing rotation
float orientationY = 30.0f;
float rotateActor = -90.0f;
float spinningRotation = 30.0f;
bool spinning = GL_TRUE;

// colors for shader
glm::vec3 objColor(1.0f, 0.5f, 0.31f);  // red
glm::vec3 planeColor(0.0f, 0.5f, 0.0f); // light green

// light components
glm::vec3 lightColor = glm::vec3(10.0f, 10.0f, 10.0f); // 10 for bloom effect and hdr 
// light position 
glm::vec3 lightPos = glm::vec3(2.0f, 13.0f, 0.0f);

// animation switcher
bool switchAnimation = true;

///////////////////////////////// STRUCTURE FOR THE ENEMIES /////////////////////////////////////////
// simulation object structure and parameters for the level structure
/* 
    every enemy is composed by 
    - aboslute pos (move the object in the battlefield)
    - relative pos (position to use to detect collision)
    - causal (motion on X or Z axis)
    - intensity (random value to modify a bit the motion)
    - hit (true if the enemy has been touched)
    - isCoin (true if the object is the crystal)
*/
struct enemieObject
{
    glm::vec3 absolutePosition;
    glm::vec3 relativePosition;
    int casual;
    double intensity;
    bool hit;
    bool isCoin;
    enemieObject(glm::vec3 p) : absolutePosition(p), relativePosition(p), casual(rand() % 2), intensity(((double)rand() / (RAND_MAX)) + 1), hit(false), isCoin(false) {}
};
//////////////////////////////////////////////////////////////////////////////////////////////////////

// bloom info
bool bloomKeyPressed = false; 
bool bloom = true;
float exposure = 1.0f;  // setted to 1.0. Modify value to obtain better illumination for darker or lighter scenes

// lifes and levels -> game starts at level 1 with 3 lifes 
int life = 3;
int current_level = 1;

// light space transform for shadow mapping 
// because we're modelling  a directional light source, all the rays are parallel 
// we use an orthographic projection matrix
float near_plane = 1.0f, far_plane = 100.5f;
glm::mat4 lightProjection = glm::ortho(-100.0f, 100.0f, -100.0f, 100.0f, near_plane, far_plane);
// light is the new point of view so we need a virtual camera to rotate the world from its prospective
glm::mat4 lightView = glm::lookAt(lightPos, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
// we multiply the two and we obtain the final matrix to obtain the light point of view 
glm::mat4 lightSpaceMatrix = lightProjection * lightView;

// ___________function declaration__________________
void processInput(GLFWwindow *window);
void framebuffer_size_callback(GLFWwindow *window, int width, int height);
// void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void KeyPressed(unsigned char key, int x, int y);
unsigned int loadTexture(char const *path, bool gammaCorrection);
std::vector<enemieObject> game_board(std::vector<enemieObject> enemies, int level);
void renderQuad();

int main()
{
    // inizialization of the window
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "Work", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window " << std::endl;
        glfwTerminate();
        return -1;
    }
    // set current thread to this window
    glfwMakeContextCurrent(window);

    // check start GLAD
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to init GLAD " << std::endl;
        return -1;
    }

    // init freetype library for text rendering
    FT_Library ft;
    if (FT_Init_FreeType(&ft))
    {
        std::cout << "ERROR::FREETYPE: Could not init FreeType Library" << std::endl;
        return -1;
    }

    FT_Face face;
    if (FT_New_Face(ft, "font/Helvetica.ttc", 0, &face))
    {
        std::cout << "ERROR::FREETYPE: Failed to load font" << std::endl;
        return -1;
    }

    // pixel font size
    FT_Set_Pixel_Sizes(face, 0, 48);

    // load glyph
    if (FT_Load_Char(face, 'X', FT_LOAD_RENDER))
    {
        std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
        return -1;
    }
    

    // setting ViewPort
    // OPENGL uses the data specified via glViewport to trasform into 2d coordinated
    // the moment the user resizes the window the viewport is adjusted as well
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    // call this function each time the mouse moves
    // glfwSetCursorPosCallback(window, mouse_callback);

    //////////////// Shaders declaration ////////////////
    Shader shader("shaders/shader.vs", "shaders/shader.fs");
    Shader skeletonShader("shaders/skeleton_animation_shader.vs", "shaders/shader.fs");
    Shader fireShader("shaders/shader.vs", "shaders/fire_shader.fs");
    Shader blurShader("shaders/blur.vs", "shaders/blur.fs");
    Shader finalShader("shaders/tex_output.vs", "shaders/tex_output.fs");
    Shader simpleDepthShader("shaders/simpleDepthShader.vs", "shaders/simpleDepthShader.fs");
    Shader simpleDepthShaderCatWarrior("shaders/simpleDepthShaderCatWarrior.vs", "shaders/simpleDepthShaderCatWarrior.fs");
    Shader debugDepthQuad("shaders/debugQuadShader.vs", "shaders/debugQuadShader.fs");
    Shader shaderShadow("shaders/shader_shadows.vs", "shaders/shader_shadows.fs");
    Shader skeletonShaderShadow("shaders/skeleton_animation_shader_shadows.vs", "shaders/shader_shadows.fs");

    //////////////// Models declaration ////////////////
    Model planeModel("../../models/plane.obj");
    Model lightModel("../../models/cube.obj");
    Model sphereCollisionModel("../../models/sphere.obj");
    Model rockModel("../../models/rocks_01_model.obj");
    Model mountainModel("../../models/mount.blend1.obj");
    Model palmModel("../../models/palm_tree.obj");
    Model leftWallModel("../../models/cube.obj");
    Model rightWallModel("../../models/cube.obj");
    Model frontWallModel("../../models/cube.obj");
    Model backWallModel("../../models/cube.obj");
    Model crystalModel("../../models/Crystal.obj");

    //////////////// Model for CatWarrior declaration ////////////////
    MeshSkin catWarrior("../../models/catwarrior/CatWarrior.gpmesh");
    Skeleton catWarriorSkeleton("../../models/catwarrior/CatWarrior.gpskel");
    Animation catWarriorIdleAnimation("../../models/catwarrior/CatActionIdle.gpanim");
    Animation catWarriorRunAnimation("../../models/catwarrior/CatRunSprint.gpanim");
    MatrixPal matrixPal;

    // identity matrix for models
    glm::mat4 planeModelMatrix = glm::mat4(1.0f);
    glm::mat4 lightModelMatrix = glm::mat4(1.0f);
    glm::mat4 catWarriorModelMatrix = glm::mat4(1.0f);
    glm::mat4 sphereCollisionModelMatrix = glm::mat4(1.0f);
    glm::mat4 rockModelMatrix = glm::mat4(1.0f);
    glm::mat4 mountainModelMatrix = glm::mat4(1.0f);
    glm::mat4 palmModelMatrix = glm::mat4(1.0f);
    glm::mat4 leftWallModelMatrix = glm::mat4(1.0f);
    glm::mat4 rightWallModelMatrix = glm::mat4(1.0f);
    glm::mat4 frontWallModelMatrix = glm::mat4(1.0f);
    glm::mat4 backWallModelMatrix = glm::mat4(1.0f);
    glm::mat4 crystalModelMatrix = glm::mat4(1.0f);

    // enable depth testing
    glEnable(GL_DEPTH_TEST);
    // focus mouse to the center of the window
    // glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);


    //////////////// Textures declaration ////////////////
    unsigned int floorTex = loadTexture("textures/rock.jpg", true);
    unsigned int catWarriorTex = loadTexture("textures/CatWarrior.png", true);
    unsigned int rockTex = loadTexture("textures/rocks_01_dif.jpg", true);
    unsigned int palmTex = loadTexture("textures/tree-bark.jpg", true);

    ////////////////////////////////////////////////////////// CREATION  //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////// OF        //////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////// LANDSCAPE //////////////////////////////////////////////////////////
    // plane info
    glm::vec3 plane_pos = glm::vec3(0.0f, -1.5f, 0.0f);
    glm::vec3 plane_size = glm::vec3(200.0f, 0.1f, 200.0f);

    // rock position
    std::vector<glm::vec3> rockPositions;
    rockPositions.push_back(glm::vec3(-140.0f, -2.5f, 130.0f));
    rockPositions.push_back(glm::vec3(-140.0f, -2.5f, 110.0f));
    rockPositions.push_back(glm::vec3(-140.0f, -2.5f, -140.0f));
    rockPositions.push_back(glm::vec3(-140.0f, -2.5f, -120.0f));
    rockPositions.push_back(glm::vec3(-60.0f, -2.5f, 140.0f));
    rockPositions.push_back(glm::vec3(-20.0f, -2.5f, 140.0f));
    rockPositions.push_back(glm::vec3(120.0f, -2.5f, 170.0f));
    rockPositions.push_back(glm::vec3(-60.0f, -2.5f, -130.0f));
    rockPositions.push_back(glm::vec3(60.0f, -2.5f, -140.0f));
    rockPositions.push_back(glm::vec3(120.0f, -2.5f, -140.0f));
    rockPositions.push_back(glm::vec3(120.0f, -2.5f, -180.0f));
    rockPositions.push_back(glm::vec3(130.0f, -2.5f, 60.0f));

    // mountain position
    std::vector<glm::vec3> mountainPosition;
    mountainPosition.push_back(glm::vec3(0.0f, -2.5f, -400.0f));
    mountainPosition.push_back(glm::vec3(50.0f, -2.5f, 400.0f));
    mountainPosition.push_back(glm::vec3(-200.0f, -2.5f, -400.0f));
    mountainPosition.push_back(glm::vec3(600.0f, -2.5f, -60.0f));
    mountainPosition.push_back(glm::vec3(600.0f, -2.5f, 400.0f));
    mountainPosition.push_back(glm::vec3(-600.0f, -2.5f, 0.0f));
    mountainPosition.push_back(glm::vec3(-600.0f, -2.5f, -90.0f));
    mountainPosition.push_back(glm::vec3(-100.0f, -2.5f, -500.0f));
    mountainPosition.push_back(glm::vec3(-300.0f, -2.5f, 300.0f));
    mountainPosition.push_back(glm::vec3(100.0f, -2.5f, -350.0f));
    mountainPosition.push_back(glm::vec3(20.0f, -2.5f, 400.0f));
    mountainPosition.push_back(glm::vec3(300.0f, -2.5f, -10.0f));
    mountainPosition.push_back(glm::vec3(700.0f, -2.5f, 0.0f));
    mountainPosition.push_back(glm::vec3(600.0f, -2.5f, -80.0f));
    mountainPosition.push_back(glm::vec3(-400.0f, -2.5f, -100.0f));
    mountainPosition.push_back(glm::vec3(-30.0f, -2.5f, -500.0f));

    // palm position
    std::vector<glm::vec3> palmPosition;
    palmPosition.push_back(glm::vec3(0.0f, -1.5f, -110.0f));

    // battlefield
    glm::vec3 leftWall_pos = glm::vec3(-100.0f, 0.0f, 0.0f);
    glm::vec3 leftWall_size = glm::vec3(1.0f, 1.0f, 100.0f);

    glm::vec3 rightWall_pos = glm::vec3(100.0f, 0.0f, 0.0f);
    glm::vec3 rightWall_size = glm::vec3(1.0f, 1.0f, 100.0f);

    glm::vec3 frontWall_pos = glm::vec3(0.0f, 0.0f, -100.0f);
    glm::vec3 frontWall_size = glm::vec3(100.0f, 1.0f, 1.0f);

    glm::vec3 backWall_pos = glm::vec3(0.0f, 0.0f, 100.0f);
    glm::vec3 backWall_size = glm::vec3(100.0f, 1.0f, 1.0f);

    ///////////////////////////////////////////////////////////////// FRAME BUFFERS ///////////////////////////////////////////////
    unsigned int hdrFBO;
    glGenFramebuffers(1, &hdrFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
    // create 2 floating point color buffers (1 for normal rendering, other for brightness treshold values)
    unsigned int colorBuffers[2];
    glGenTextures(2, colorBuffers);
    for (unsigned int i = 0; i < 2; i++)
    {
        glBindTexture(GL_TEXTURE_2D, colorBuffers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, WIDTH * 2, HEIGHT * 2, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // we clamp to the edge as the blur filter would otherwise sample repeated texture values
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        // attach texture to framebuffer
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorBuffers[i], 0);
    }
    // create and attach depth buffer 
    unsigned int rboDepth;
    glGenRenderbuffers(1, &rboDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, WIDTH * 2, HEIGHT * 2);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
    // attachment we will use 
    unsigned int attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, attachments);
    // check if framebuffer is complete
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // ping-pong-framebuffer for blurring
    unsigned int pingpongFBO[2];
    unsigned int pingpongColorbuffers[2];
    glGenFramebuffers(2, pingpongFBO);
    glGenTextures(2, pingpongColorbuffers);
    for (unsigned int i = 0; i < 2; i++)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[i]);
        glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, WIDTH * 2, HEIGHT * 2, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingpongColorbuffers[i], 0);
        // also check if framebuffers are complete 
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            std::cout << "Framebuffer not complete!" << std::endl;
    }

    // depth map for shadows
    unsigned int depthMapFBO;
    glGenFramebuffers(1, &depthMapFBO);
    const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

    unsigned int depthMap;
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    // here we use shadow sizes 
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH * 2, SHADOW_HEIGHT * 2, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    // fix peter panning error in shadow mapping 
    float borderColor[] = {1.0, 1.0, 1.0, 1.0};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    // attach as the frameBuffer's depth buffer
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
    glDrawBuffer(GL_NONE); // ->  we specify that we won't use color info but only the depth info
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // info for shader
    shader.use();
    shader.setVec3("lightPos", lightPos);
    shader.setVec3("lightColor", lightColor);
    shader.setVec3("viewPos", camera.Position);
    shader.setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    shader.setVec3("light.diffuse", 0.5f, 0.5f, 0.5f);
    shader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);
    shader.setInt("diffuseTex", 0);
    // info for shader shadows
    shaderShadow.use();
    shaderShadow.setVec3("lightPos", lightPos);
    shaderShadow.setVec3("lightColor", lightColor);
    shaderShadow.setVec3("viewPos", camera.Position);
    shaderShadow.setVec3("light.ambient", 0.3f, 0.3f, 0.3f);
    shaderShadow.setVec3("light.diffuse", 0.6f, 0.6f, 0.6f);
    shaderShadow.setVec3("light.specular", 1.0f, 1.0f, 1.0f);
    shaderShadow.setInt("diffuseTex", 0);
    shaderShadow.setInt("shadowMap", 1);
    // info for skeleton
    skeletonShader.use();
    skeletonShader.setVec3("lightPos", lightPos);
    skeletonShader.setVec3("light.ambient", 0.2f, 0.2f, 0.2f);
    skeletonShader.setVec3("light.diffuse", 0.5f, 0.5f, 0.5f);
    skeletonShader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);
    skeletonShader.setVec3("ColorIn", objColor);
    skeletonShader.setFloat("repetition", 1);
    skeletonShader.setVec3("materialTexture.specular", 0.5f, 0.5f, 0.5f);
    skeletonShader.setFloat("materialTexture.shininess", 32.0f);
    // info for shadow skeleton
    skeletonShaderShadow.use();
    skeletonShaderShadow.setVec3("lightPos", lightPos);
    skeletonShaderShadow.setVec3("light.ambient", 0.3f, 0.3f, 0.3f);
    skeletonShaderShadow.setVec3("light.diffuse", 0.6f, 0.6f, 0.6f);
    skeletonShaderShadow.setVec3("light.specular", 1.0f, 1.0f, 1.0f);
    skeletonShaderShadow.setVec3("ColorIn", objColor);
    skeletonShaderShadow.setFloat("repetition", 1);
    skeletonShaderShadow.setVec3("materialTexture.specular", 0.5f, 0.5f, 0.5f);
    skeletonShaderShadow.setFloat("materialTexture.shininess", 32.0f);
    skeletonShaderShadow.setInt("diffuseTex", 0);
    skeletonShaderShadow.setInt("shadowMap", 1);
    // info for blur shader
    blurShader.use();
    blurShader.setInt("image", 0);
    // info for final render shader
    finalShader.use();
    finalShader.setInt("scene", 0);
    finalShader.setInt("bloomBlur", 1);
    // info for the debug shader -> used only for debugging
    debugDepthQuad.use();
    debugDepthQuad.setInt("depthMap", 0);

    // vector for enemies 
    std::vector<enemieObject> enemies;
    // matrix of integers for the positioning of enemies in the battlefield
    std::vector<std::vector<int>> matrix_level;

    // init game board
    enemies = game_board(enemies, current_level);

    // projection matrix + view matrix declaration
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)WIDTH / (float)HEIGHT, 0.1f, 1000.0f); // 0.1 and 100 specify the near and far plane of the frustum
    glm::mat4 view;

    // render loop
    // glfwWindowShouldClose(window) checks each loop if glwf has been instructed to close
    while (!glfwWindowShouldClose(window))
    {
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // process inputs
        processInput(window);

        // render commands || GL_COLOR_BUFFER_BIT specifies the buffer to clear
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////// BLOOM + HDR ////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////


        if (bloom)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            // simple shader 
            shader.use();

            // set projection matrix 
            shader.setMat4("projectionMatrix", projection);

            // get view matrix for shader
            view = camera.GetViewMatrix();
            shader.setMat4("viewMatrix", view);

            ////////////////////////////////////////// LIGHT MODEL ////////////////////////////////////////////////////////
            unsigned int index = glGetSubroutineIndex(shader.ID, GL_FRAGMENT_SHADER, "drawLight");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            lightModelMatrix = glm::mat4(1.0f);
            lightModelMatrix = glm::translate(lightModelMatrix, lightPos);
            lightModelMatrix = glm::scale(lightModelMatrix, glm::vec3(0.5f, 0.5f, 0.5f));
            shader.setMat4("modelMatrix", lightModelMatrix);
            lightModel.Draw();

            ////////////////////////////////////////// AMBIENT MODELS ////////////////////////////////////////////////////////
            index = glGetSubroutineIndex(shader.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            // material of the room
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, floorTex);

            // repetition for the terrain 
            shader.setFloat("repetition", 100);
            shader.setVec3("materialTexture.specular", 0.1f, 0.1f, 0.1f);
            shader.setFloat("materialTexture.shininess", 256.0f);

            // floor
            planeModelMatrix = glm::mat4(1.0f);
            planeModelMatrix = glm::translate(planeModelMatrix, plane_pos);
            planeModelMatrix = glm::scale(planeModelMatrix, plane_size);
            shader.setMat4("modelMatrix", planeModelMatrix);
            planeModel.Draw();

            // mountain model
            shader.setFloat("repetition", 5);
            // draw mountains 
            for (int mnt = 0; mnt < mountainPosition.size(); mnt++)
            {
                mountainModelMatrix = glm::mat4(1.0f);
                mountainModelMatrix = glm::translate(mountainModelMatrix, mountainPosition[mnt]);
                mountainModelMatrix = glm::scale(mountainModelMatrix, glm::vec3(30.0f, 30.0f, 30.0f));
                shader.setMat4("modelMatrix", mountainModelMatrix);
                mountainModel.Draw();
            }

            // palm model
            glBindTexture(GL_TEXTURE_2D, palmTex);
            shader.setFloat("repetition", 3);
            // draw palms 
            for (int pl = 0; pl < palmPosition.size(); pl++)
            {
                palmModelMatrix = glm::mat4(1.0f);
                palmModelMatrix = glm::translate(palmModelMatrix, palmPosition[pl]);
                palmModelMatrix = glm::scale(palmModelMatrix, glm::vec3(3.0f, 3.0f, 3.0f));
                shader.setMat4("modelMatrix", palmModelMatrix);
                palmModel.Draw();
            }

            // rock model
            index = glGetSubroutineIndex(shader.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            // material for rocks
            glBindTexture(GL_TEXTURE_2D, rockTex);
            shader.setFloat("repetition", 1);

            // draw rocks
            for (int rock = 0; rock < rockPositions.size(); rock++)
            {
                rockModelMatrix = glm::mat4(1.0f);
                rockModelMatrix = glm::translate(rockModelMatrix, rockPositions[rock]);
                rockModelMatrix = glm::scale(rockModelMatrix, glm::vec3(0.1f, 0.1f, 0.1f));
                shader.setMat4("modelMatrix", rockModelMatrix);
                rockModel.Draw();
            }

            // shader for fire effects of the balls 
            fireShader.use();
            fireShader.setMat4("projectionMatrix", projection);
            // get view matrix 
            view = camera.GetViewMatrix();
            fireShader.setMat4("viewMatrix", view);

            // fireshader is based on the time and resolution of the object
            fireShader.setFloat("time", currentFrame);
            fireShader.setVec2("resolution", glm::vec2(2.0f, 2.0f));
            // color fire selected = red 
            fireShader.setBool("choose_color_fire", true);

            // draw all the enemies
            for (int x = 0; x < enemies.size(); x++)
            {
                // check if the enemy is the crystal 
                // false = draw enemy
                // true = draw Crystal 
                if (!enemies[x].isCoin)
                {
                    // draw only if it has not be hitten 
                    if (!enemies[x].hit)
                    {
                        sphereCollisionModelMatrix = glm::mat4(1.0f);
                        // check for moving along the X or Z axis 
                        if (enemies[x].casual == 0)
                        {
                            // object is moved with trigonometric function = sin 
                            // the movement is given by the product of the position x sin(Time) x intensity
                            sphereCollisionModelMatrix = glm::translate(sphereCollisionModelMatrix, glm::vec3(enemies[x].absolutePosition.x * sin(glfwGetTime()) * enemies[x].intensity, enemies[x].absolutePosition.y, enemies[x].absolutePosition.z));
                            enemies[x].relativePosition.x = enemies[x].absolutePosition.x * sin(glfwGetTime()) * enemies[x].intensity;
                        }
                        else if (enemies[x].casual == 1)
                        {
                            sphereCollisionModelMatrix = glm::translate(sphereCollisionModelMatrix, glm::vec3(enemies[x].absolutePosition.x, enemies[x].absolutePosition.y, enemies[x].absolutePosition.z * sin(glfwGetTime()) * enemies[x].intensity));
                            enemies[x].relativePosition.z = enemies[x].absolutePosition.z * sin(glfwGetTime()) * enemies[x].intensity;
                        }
                        sphereCollisionModelMatrix = glm::scale(sphereCollisionModelMatrix, glm::vec3(1.5f, 1.5f, 1.5f));
                        fireShader.setMat4("modelMatrix", sphereCollisionModelMatrix);
                        sphereCollisionModel.Draw();
                    }
                }
                else
                {
                    // if the enemy is the crystal, we draw it with a pink color and without texture 
                    shader.use();
                    index = glGetSubroutineIndex(shader.ID, GL_FRAGMENT_SHADER, "drawNoTex");
                    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);
                    shader.setVec3("material.ambient", 10.0f, 0.0f, 10.0f);
                    shader.setVec3("material.diffuse", 10.0f, 0.0f, 10.0f);
                    shader.setVec3("material.specular", 10.0f, 0.0f, 10.0f);
                    shader.setFloat("material.shininess", 32.0f);

                    crystalModelMatrix = glm::mat4(1.0f);
                    crystalModelMatrix = glm::translate(crystalModelMatrix, enemies[x].absolutePosition);
                    shader.setMat4("modelMatrix", crystalModelMatrix);
                    crystalModel.Draw();
                }
            }

            // try to reproduce an electrified ring
            fireShader.use();
            fireShader.setVec2("resolution", glm::vec2(100.0f, 2.0f));
            // color fire selected = blue 
            fireShader.setBool("choose_color_fire", false);

            // left_battelfield
            leftWallModelMatrix = glm::mat4(1.0f);
            leftWallModelMatrix = glm::translate(leftWallModelMatrix, leftWall_pos);
            leftWallModelMatrix = glm::scale(leftWallModelMatrix, leftWall_size);
            fireShader.setMat4("modelMatrix", leftWallModelMatrix);
            leftWallModel.Draw();

            // right_battelfield
            rightWallModelMatrix = glm::mat4(1.0f);
            rightWallModelMatrix = glm::translate(rightWallModelMatrix, rightWall_pos);
            rightWallModelMatrix = glm::scale(rightWallModelMatrix, rightWall_size);
            fireShader.setMat4("modelMatrix", rightWallModelMatrix);
            rightWallModel.Draw();

            // front_battelfield
            frontWallModelMatrix = glm::mat4(1.0f);
            frontWallModelMatrix = glm::translate(frontWallModelMatrix, frontWall_pos);
            frontWallModelMatrix = glm::scale(frontWallModelMatrix, frontWall_size);
            fireShader.setMat4("modelMatrix", frontWallModelMatrix);
            frontWallModel.Draw();

            // back_battelfield
            backWallModelMatrix = glm::mat4(1.0f);
            backWallModelMatrix = glm::translate(backWallModelMatrix, backWall_pos);
            backWallModelMatrix = glm::scale(backWallModelMatrix, backWall_size);
            fireShader.setMat4("modelMatrix", backWallModelMatrix);
            backWallModel.Draw();

            ////////////////////////////////////////// CATWARRIOR MODEL //////////////////////////////////////////////////////
            // global variable for shader
            skeletonShader.use();

            // projection matrix for the catwarrior
            skeletonShader.setMat4("projectionMatrix", projection);

            view = camera.GetViewMatrix();
            skeletonShader.setMat4("viewMatrix", view);
            skeletonShader.setVec3("viewPos", camera.Position);

            // bind catwarrior texture 
            glBindTexture(GL_TEXTURE_2D, catWarriorTex);

            index = glGetSubroutineIndex(skeletonShader.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            
            catWarriorModelMatrix = glm::mat4(1.0f);
            catWarriorModelMatrix = glm::translate(catWarriorModelMatrix, camera.ownerPos);
            // the rotations come from  
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(1.0f, 0.0f, 0.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(0.0f, 0.0f, -1.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(camera.rotateActor), glm::vec3(0.0f, 0.0f, 1.0f));

            catWarriorModelMatrix = glm::scale(catWarriorModelMatrix, glm::vec3(0.03f, 0.03f, 0.03f));

            skeletonShader.setMat4("modelMatrix", catWarriorModelMatrix);

            // switchAnimation is used to switch between the animation of the run and the animation for the respiration
            // fill the matrix Palette with the transformation matrix to rotate the model 
            if (switchAnimation)
            {
                matrixPal.updateTime(deltaTime, catWarriorIdleAnimation);
                matrixPal.computeMatrixPalette(catWarriorSkeleton, catWarriorIdleAnimation);
            }
            else
            {
                matrixPal.updateTime(deltaTime, catWarriorRunAnimation);
                matrixPal.computeMatrixPalette(catWarriorSkeleton, catWarriorRunAnimation);
            }

            // passing the matrix Palette to the shader for the catWarrior
            GLuint loc = glGetUniformLocation(skeletonShader.ID, "uMatrixPalette");
            glUniformMatrix4fv(loc, MAX_SKELETON_BONES, GL_FALSE, glm::value_ptr(matrixPal.MatrixPalette[0]));

            catWarrior.Draw();

            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            
            ////////////////////////////////////////// FINAL RENDERING //////////////////////////////////////////////////////
            // two pass Gaussian Blur 

            // if horizontal = true blur orizzontally else vertically
            bool horizontal = true, f_iteration = true;
            // ten times || 5 per part
            unsigned int amount = 10;

            blurShader.use();
            for (unsigned int i = 0; i < amount; i++)
            {
                glBindFramebuffer(GL_FRAMEBUFFER, pingpongFBO[horizontal]);
                blurShader.setInt("horizontal", horizontal);
                // if first iterarion we take colorBuffer else we use ping pong buffers 
                glBindTexture(GL_TEXTURE_2D, f_iteration ? colorBuffers[1] : pingpongColorbuffers[!horizontal]); // bind texture of other framebuffer (or scene if first iteration)
                renderQuad();
                horizontal = !horizontal;
                if (f_iteration)
                    f_iteration = false;
            }

            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            // clear buffers 
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            finalShader.use();
            // union of the two textures and print to cover the screen 
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, colorBuffers[0]);
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, pingpongColorbuffers[!horizontal]);
            finalShader.setFloat("exposure", exposure);
            renderQuad();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////// SHADOW MAPPING /////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////

        else
        {
            ////////////////////////////////////////// FILL DEPTH MAP ////////////////////////////////////////////////
            // ------------------------------------------------------------
            // we render the scene first from the light point of view, so we will use not the usual viewMatrix 
            // but the lightSpaceMatrix
            // ------------------------------------------------------------

            glViewport(0, 0, SHADOW_WIDTH * 2, SHADOW_HEIGHT * 2);
            simpleDepthShader.use();

            // field of view angle , aspect ratio
            simpleDepthShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);

            // clean depth buffer and start drawing into the texture
            glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
            glClear(GL_DEPTH_BUFFER_BIT);

            ////////////////////////////////////////// MODELS ////////////////////////////////////////////////////////

            // floor
            planeModelMatrix = glm::mat4(1.0f);
            planeModelMatrix = glm::translate(planeModelMatrix, plane_pos);
            planeModelMatrix = glm::scale(planeModelMatrix, plane_size);
            simpleDepthShader.setMat4("modelMatrix", planeModelMatrix);
            planeModel.Draw();

            // left_battelfield
            leftWallModelMatrix = glm::mat4(1.0f);
            leftWallModelMatrix = glm::translate(leftWallModelMatrix, leftWall_pos);
            leftWallModelMatrix = glm::scale(leftWallModelMatrix, leftWall_size);
            simpleDepthShader.setMat4("modelMatrix", leftWallModelMatrix);
            leftWallModel.Draw();

            // right_battelfield
            rightWallModelMatrix = glm::mat4(1.0f);
            rightWallModelMatrix = glm::translate(rightWallModelMatrix, rightWall_pos);
            rightWallModelMatrix = glm::scale(rightWallModelMatrix, rightWall_size);
            simpleDepthShader.setMat4("modelMatrix", rightWallModelMatrix);
            rightWallModel.Draw();

            // front_battelfield
            frontWallModelMatrix = glm::mat4(1.0f);
            frontWallModelMatrix = glm::translate(frontWallModelMatrix, frontWall_pos);
            frontWallModelMatrix = glm::scale(frontWallModelMatrix, frontWall_size);
            simpleDepthShader.setMat4("modelMatrix", frontWallModelMatrix);
            frontWallModel.Draw();

            // back_battelfield
            backWallModelMatrix = glm::mat4(1.0f);
            backWallModelMatrix = glm::translate(backWallModelMatrix, backWall_pos);
            backWallModelMatrix = glm::scale(backWallModelMatrix, backWall_size);
            simpleDepthShader.setMat4("modelMatrix", backWallModelMatrix);
            backWallModel.Draw();

            ////////////////////////////////////////// CATWARRIOR MODEL //////////////////////////////////////////////////////

            simpleDepthShaderCatWarrior.use();

            // field of view angle , aspect ratio
            simpleDepthShaderCatWarrior.setMat4("lightSpaceMatrix", lightSpaceMatrix);
            // cat warrior loading model
            catWarriorModelMatrix = glm::mat4(1.0f);

            catWarriorModelMatrix = glm::translate(catWarriorModelMatrix, camera.ownerPos);
            // We rotate the model because its start orientation is rotated along the axis of X and Z of 90 degrees 
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(1.0f, 0.0f, 0.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(0.0f, 0.0f, -1.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(camera.rotateActor), glm::vec3(0.0f, 0.0f, 1.0f));

            catWarriorModelMatrix = glm::scale(catWarriorModelMatrix, glm::vec3(0.03f, 0.03f, 0.03f));

            simpleDepthShaderCatWarrior.setMat4("modelMatrix", catWarriorModelMatrix);

            // switchAnimation is used to switch between the animation of the run and the animation for the respiration
            // fill the matrix Palette with the transformation matrix to rotate the model 
            if (switchAnimation)
            {
                matrixPal.updateTime(deltaTime, catWarriorIdleAnimation);
                matrixPal.computeMatrixPalette(catWarriorSkeleton, catWarriorIdleAnimation);
            }
            else
            {
                matrixPal.updateTime(deltaTime, catWarriorRunAnimation);
                matrixPal.computeMatrixPalette(catWarriorSkeleton, catWarriorRunAnimation);
            }
            // passing the matrix Palette to the shader for the catWarrior
            GLuint loc = glGetUniformLocation(simpleDepthShaderCatWarrior.ID, "uMatrixPalette");
            glUniformMatrix4fv(loc, MAX_SKELETON_BONES, GL_FALSE, glm::value_ptr(matrixPal.MatrixPalette[0]));

            catWarrior.Draw();
            //
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            // reset view port

            glViewport(0, 0, WIDTH * 2, HEIGHT * 2);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            //////////////////////// RENDER SCENE FROM CAMERA PERSPECTIVE ////////////////////////////////////////////////

            shaderShadow.use();
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, depthMap);

            // field of view angle , aspect ratio
            shaderShadow.setMat4("projectionMatrix", projection);

            // view matrix for shader
            view = camera.GetViewMatrix();
            shaderShadow.setMat4("viewMatrix", view);

            // light space matrix
            shaderShadow.setMat4("lightSpaceMatrix", lightSpaceMatrix);

            ////////////////////////////////////////// LIGHT MODEL ////////////////////////////////////////////////////////
            unsigned int index = glGetSubroutineIndex(shaderShadow.ID, GL_FRAGMENT_SHADER, "drawLight");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            lightModelMatrix = glm::mat4(1.0f);
            lightModelMatrix = glm::translate(lightModelMatrix, lightPos);
            lightModelMatrix = glm::scale(lightModelMatrix, glm::vec3(0.5f, 0.5f, 0.5f));
            shaderShadow.setMat4("modelMatrix", lightModelMatrix);
            lightModel.Draw();

            ////////////////////////////////////////// ROOM MODELS ////////////////////////////////////////////////////////

            index = glGetSubroutineIndex(shaderShadow.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            // material of the room
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, floorTex);

            shaderShadow.setFloat("repetition", 100);
            shaderShadow.setVec3("materialTexture.specular", 0.1f, 0.1f, 0.1f);
            shaderShadow.setFloat("materialTexture.shininess", 256.0f);

            // floor
            planeModelMatrix = glm::mat4(1.0f);
            planeModelMatrix = glm::translate(planeModelMatrix, plane_pos);
            planeModelMatrix = glm::scale(planeModelMatrix, plane_size);
            shaderShadow.setMat4("modelMatrix", planeModelMatrix);
            planeModel.Draw();

            // mountain model
            shaderShadow.setFloat("repetition", 5);

            for (int mnt = 0; mnt < mountainPosition.size(); mnt++)
            {
                mountainModelMatrix = glm::mat4(1.0f);
                mountainModelMatrix = glm::translate(mountainModelMatrix, mountainPosition[mnt]);
                mountainModelMatrix = glm::scale(mountainModelMatrix, glm::vec3(30.0f, 30.0f, 30.0f));
                shaderShadow.setMat4("modelMatrix", mountainModelMatrix);
                mountainModel.Draw();
            }

            // palm model
            glBindTexture(GL_TEXTURE_2D, palmTex);
            shaderShadow.setFloat("repetition", 3);

            for (int pl = 0; pl < palmPosition.size(); pl++)
            {
                palmModelMatrix = glm::mat4(1.0f);
                palmModelMatrix = glm::translate(palmModelMatrix, palmPosition[pl]);
                palmModelMatrix = glm::scale(palmModelMatrix, glm::vec3(3.0f, 3.0f, 3.0f));
                shaderShadow.setMat4("modelMatrix", palmModelMatrix);
                palmModel.Draw();
            }

            // rocks model
            index = glGetSubroutineIndex(shaderShadow.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            // material for rocks
            glBindTexture(GL_TEXTURE_2D, rockTex);
            shaderShadow.setFloat("repetition", 1);

            for (int rock = 0; rock < rockPositions.size(); rock++)
            {
                rockModelMatrix = glm::mat4(1.0f);
                rockModelMatrix = glm::translate(rockModelMatrix, rockPositions[rock]);
                rockModelMatrix = glm::scale(rockModelMatrix, glm::vec3(0.1f, 0.1f, 0.1f));
                shaderShadow.setMat4("modelMatrix", rockModelMatrix);
                rockModel.Draw();
            }

            // shader for fire effects
            fireShader.use();
            fireShader.setMat4("projectionMatrix", projection);
            view = camera.GetViewMatrix();
            fireShader.setMat4("viewMatrix", view);

            fireShader.setFloat("time", currentFrame);
            fireShader.setVec2("resolution", glm::vec2(2.0f, 2.0f));
            fireShader.setBool("choose_color_fire", true);

            // draw all the enemies, if the enemies is the coin draw the coin instead of the enemies
            for (int x = 0; x < enemies.size(); x++)
            {
                if (!enemies[x].isCoin)
                {
                    if (!enemies[x].hit)
                    {
                        sphereCollisionModelMatrix = glm::mat4(1.0f);
                        if (enemies[x].casual == 0)
                        {
                            sphereCollisionModelMatrix = glm::translate(sphereCollisionModelMatrix, glm::vec3(enemies[x].absolutePosition.x * sin(glfwGetTime()) * enemies[x].intensity, enemies[x].absolutePosition.y, enemies[x].absolutePosition.z));
                            enemies[x].relativePosition.x = enemies[x].absolutePosition.x * sin(glfwGetTime()) * enemies[x].intensity;
                        }
                        else if (enemies[x].casual == 1)
                        {
                            sphereCollisionModelMatrix = glm::translate(sphereCollisionModelMatrix, glm::vec3(enemies[x].absolutePosition.x, enemies[x].absolutePosition.y, enemies[x].absolutePosition.z * sin(glfwGetTime()) * enemies[x].intensity));
                            enemies[x].relativePosition.z = enemies[x].absolutePosition.z * sin(glfwGetTime()) * enemies[x].intensity;
                        }
                        sphereCollisionModelMatrix = glm::scale(sphereCollisionModelMatrix, glm::vec3(1.5f, 1.5f, 1.5f));
                        fireShader.setMat4("modelMatrix", sphereCollisionModelMatrix);
                        sphereCollisionModel.Draw();
                    }
                }
                else
                {
                    shaderShadow.use();
                    index = glGetSubroutineIndex(shaderShadow.ID, GL_FRAGMENT_SHADER, "drawNoTex");
                    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);
                    shaderShadow.setVec3("material.ambient", 10.0f, 0.0f, 10.0f);
                    shaderShadow.setVec3("material.diffuse", 10.0f, 0.0f, 10.0f);
                    shaderShadow.setVec3("material.specular", 10.0f, 0.0f, 10.0f);
                    shaderShadow.setFloat("material.shininess", 32.0f);

                    crystalModelMatrix = glm::mat4(1.0f);
                    crystalModelMatrix = glm::translate(crystalModelMatrix, enemies[x].absolutePosition);
                    shaderShadow.setMat4("modelMatrix", crystalModelMatrix);
                    crystalModel.Draw();
                }
            }

            // try to reproduce the electrified ring
            fireShader.use();
            fireShader.setVec2("resolution", glm::vec2(100.0f, 2.0f));
            fireShader.setBool("choose_color_fire", false);

            // left_battelfield
            leftWallModelMatrix = glm::mat4(1.0f);
            leftWallModelMatrix = glm::translate(leftWallModelMatrix, leftWall_pos);
            leftWallModelMatrix = glm::scale(leftWallModelMatrix, leftWall_size);
            fireShader.setMat4("modelMatrix", leftWallModelMatrix);
            leftWallModel.Draw();

            // right_battelfield
            rightWallModelMatrix = glm::mat4(1.0f);
            rightWallModelMatrix = glm::translate(rightWallModelMatrix, rightWall_pos);
            rightWallModelMatrix = glm::scale(rightWallModelMatrix, rightWall_size);
            fireShader.setMat4("modelMatrix", rightWallModelMatrix);
            rightWallModel.Draw();

            // front_battelfield
            frontWallModelMatrix = glm::mat4(1.0f);
            frontWallModelMatrix = glm::translate(frontWallModelMatrix, frontWall_pos);
            frontWallModelMatrix = glm::scale(frontWallModelMatrix, frontWall_size);
            fireShader.setMat4("modelMatrix", frontWallModelMatrix);
            frontWallModel.Draw();

            // back_battelfield
            backWallModelMatrix = glm::mat4(1.0f);
            backWallModelMatrix = glm::translate(backWallModelMatrix, backWall_pos);
            backWallModelMatrix = glm::scale(backWallModelMatrix, backWall_size);
            fireShader.setMat4("modelMatrix", backWallModelMatrix);
            backWallModel.Draw();

            ////////////////////////////////////////// CATWARRIOR MODEL //////////////////////////////////////////////////////
            // global variable for shader
            skeletonShaderShadow.use();

            // field of view angle , aspect ratio
            skeletonShaderShadow.setMat4("projectionMatrix", projection);
            skeletonShaderShadow.setMat4("lightSpaceMatrix", lightSpaceMatrix);

            view = camera.GetViewMatrix();
            skeletonShaderShadow.setMat4("viewMatrix", view);
            skeletonShaderShadow.setVec3("viewPos", camera.Position);

            glBindTexture(GL_TEXTURE_2D, catWarriorTex);

            index = glGetSubroutineIndex(skeletonShaderShadow.ID, GL_FRAGMENT_SHADER, "Blinn_Phong");
            glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &index);

            // cat warrior loading model
            catWarriorModelMatrix = glm::mat4(1.0f);

            catWarriorModelMatrix = glm::translate(catWarriorModelMatrix, camera.ownerPos);
            // the rotations are needed because the mesh of the original model is based not on x y z
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(1.0f, 0.0f, 0.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(rotateActor), glm::vec3(0.0f, 0.0f, -1.0f));
            catWarriorModelMatrix = glm::rotate(catWarriorModelMatrix, glm::radians(camera.rotateActor), glm::vec3(0.0f, 0.0f, 1.0f));

            catWarriorModelMatrix = glm::scale(catWarriorModelMatrix, glm::vec3(0.03f, 0.03f, 0.03f));

            skeletonShaderShadow.setMat4("modelMatrix", catWarriorModelMatrix);

            loc = glGetUniformLocation(skeletonShaderShadow.ID, "uMatrixPalette");
            glUniformMatrix4fv(loc, MAX_SKELETON_BONES, GL_FALSE, glm::value_ptr(matrixPal.MatrixPalette[0]));
            catWarrior.Draw();

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // this code is not used, decomment the final line to check the correct display of the depth map
            debugDepthQuad.use();
            debugDepthQuad.setFloat("near_plane", near_plane);
            debugDepthQuad.setFloat("far_plane", far_plane);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, depthMap);
            // renderQuad();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////// CHECK COLLISION //////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        // check collision, brut force 
        for (int contact = 0; contact < enemies.size(); contact++)
        {
            // check only if the ball has never touched
            if (!enemies[contact].hit)
            {
                // check x-axis && z-axis
                if (abs(camera.ownerPos.x - enemies[contact].relativePosition.x) <= MIN_DISTANCE && abs(camera.ownerPos.z - enemies[contact].relativePosition.z) <= MIN_DISTANCE)
                {
                    // there is a contact
                    // set the enemies hit variable to true 
                    enemies[contact].hit = true;
                    // if the enemy is the crystal then ...
                    if (enemies[contact].isCoin)
                    {
                        // start new level and recreate enemies on the battlefield 
                        enemies.clear();
                        enemies = game_board(enemies, ++current_level);
                        std::cout << "Level = " << current_level << std::endl;
                    }
                    // if it was an enemy we have lost a life 
                    else
                    {
                        life = life - 1;
                        std::cout << "Life = " << life << std::endl;
                    }
                }
            }
        }

        // check status of lifes 
        if (life == 0){
            // the player is dead 
            glfwSetWindowShouldClose(window, true);
        }

        // swap color buffer
        glfwSwapBuffers(window);

        // checks if any events are triggered, update window state.
        glfwPollEvents();
    }

    // close glfw
    glfwTerminate();
    return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// dedicated to keyboard
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    // manage polygonal mode
    if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
    {
        if (!polygonalMode)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            polygonalMode = true;
        }
        else
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            polygonalMode = false;
        }
    }

    // press space for using normal light or bloom effect 
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && !bloomKeyPressed)
    {
        bloom = !bloom;
        bloomKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE)
    {
        bloomKeyPressed = false;
    }

    // WASD movement
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        camera.ProcessKeyboard(FORWARD, deltaTime);
        switchAnimation = false;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_RELEASE)
    {
        switchAnimation = true;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

// dedicated to mouse events
/*
void mouse_callback(GLFWwindow *window, double xpos, double ypos)
{
    // xpos and ypos detect the difference respect the last position of the mouse
    // they start in the center of the screen
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    // calculate the difference based respect the last movement
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}
*/

// code for loading textures
unsigned int loadTexture(char const *path, bool gammaCorrection)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int w, h, nrC;
    unsigned char *data = stbi_load(path, &w, &h, &nrC, 0);
    if (data)
    {
        GLenum format;
        if (nrC == 1)
            format = GL_RED;
        else if (nrC == 3)
            format = GL_RGB;
        else if (nrC == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, w, h, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        // we set how to consider UVs outside [0,1] range
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // we set the filtering for minification and magnification
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "texture failed to load at path " << path << std::endl;
        stbi_image_free(data);
    }
    return textureID;
}

/* 
    The function takes in input the empty vector of enemies and the level we are.
    It returns a full vector of enemies.
*/
std::vector<enemieObject> game_board(std::vector<enemieObject> enemies, int level)
{
    // creation of the matrix for storing info about the position of the enemies 
    std::vector<std::vector<int>> matrix_level;

    // level 
    const int lv = level;

    // resize matrix 
    matrix_level.resize(lv);
    for (int i = 0; i < lv; ++i)
    {
        // grow Columns by n
        matrix_level[i].resize(lv);
    }

    // create positions randomly
    // before we create a matrix of 0 and 1, 0 = no enemy ; 1 = enemy 
    // then we translate the matrix on the battlefield and we get the positions 
    for (int i = 0; i < lv; i++)
    {
        for (int j = 0; j < lv; j++)
        {
            matrix_level[i][j] = rand() % 2;
            // std::cout << matrix_level[i][j] << std::endl;
            if (matrix_level[i][j] == 1)
            {
                float w, p;
                // proportion 
                // i : lv = x : plane table         -> x = ? -> x = (i * plane table) / lv 
                w = (i * PLANE_TABLE) / lv;
                p = (j * PLANE_TABLE) / lv;
                // because the battlefield is not centered 
                w = w - (PLANE_TABLE / 2);
                p = -p + (PLANE_TABLE / 2);
                enemies.push_back(enemieObject(glm::vec3(w, 2.0f, p)));
            }
        }
    }

    // if all the values in the matrix results 0 then we create manually a first object that it will be the crystal
    if (enemies.size() == 0)
    {
        enemies.push_back(enemieObject(glm::vec3(10.0f, 2.0f, -10.0f)));
    }
    
    // resize of the intensity if too much strong 
    for (int v = 0; v < enemies.size(); v++)
        if (enemies[v].intensity > 0.5)
            enemies[v].intensity = 0.5;

    // randomly choose 1 enemy and trasnform it into the crystal 
    // if there is just one enemy it will be the crystal and there will not be 
    // enemies on the battlefield
    int casual_coin = rand() % enemies.size();
    enemies[casual_coin].isCoin = true;

    return enemies;
}

// renderQuad() renders a 1x1 XY quad 
// ----------------------------------
unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
    if (quadVAO == 0)
    {
        float quadVertices[] = {
            -1.0f,
            1.0f,
            0.0f,
            0.0f,
            1.0f,
            -1.0f,
            -1.0f,
            0.0f,
            0.0f,
            0.0f,
            1.0f,
            1.0f,
            0.0f,
            1.0f,
            1.0f,
            1.0f,
            -1.0f,
            0.0f,
            1.0f,
            0.0f,
        };
        // setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}
